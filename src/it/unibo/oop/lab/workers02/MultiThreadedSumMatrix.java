package it.unibo.oop.lab.workers02;
import java.util.*;


public class MultiThreadedSumMatrix implements SumMatrix {
    private final int nthread;
    private double res;
    
    public MultiThreadedSumMatrix(final int nthread) {
        this.nthread = nthread;
        this.res = 0;
    }
    
    private final class Worker extends Thread {
        private final double[][] matrix;
        private final int startpos;
        private final int finalpos;
        private final int npos;
        private double workres;
        
        public Worker(final double[][] matrix, final int startpos, final int npos) {
            this.matrix = matrix;
            this.startpos = startpos;
            this.npos = npos;
            this.finalpos = this.startpos + this.npos;
            this.workres = 0;
        }
        
        public void run() {
            for (int i = this.startpos; i < this.finalpos; i++) {
                for (double tmp : matrix[i]) {
                    this.workres += tmp;
                }
            }
            MultiThreadedSumMatrix.this.putres(this.workres);
        }
    }
    
    private synchronized void putres(final double workres) {
        this.res += workres;
    }
    
    @Override
    public double sum(double[][] matrix) {
        final List<Worker> workers = new ArrayList<>(nthread);
        final int posperw = matrix.length / this.nthread;
        final int finalnum = (matrix.length / this.nthread) + (matrix.length % this.nthread);
        int currentpos = 0;
        
        for (int i = 0; i < (this.nthread - 1); i++) {
            workers.add(new Worker(matrix, currentpos, posperw));
            currentpos += posperw;
        }
        workers.add(new Worker(matrix, currentpos, finalnum));
        for (Worker worker : workers) {
            worker.start();
        }
        for (Worker worker : workers) {
            try {
                worker.join();
            } catch (InterruptedException e) {
                System.out.println("error");
            }
        }
        System.out.println("my result: " + this.res);
        return this.res;
    }
}







