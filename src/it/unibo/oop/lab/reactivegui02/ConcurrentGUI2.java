package it.unibo.oop.lab.reactivegui02;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ConcurrentGUI2 extends JFrame {
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    private final ExecutorService pool;

    /**
     * Builds a new CGUI.
     */
    public ConcurrentGUI2() {
        super();
        this.pool = Executors.newSingleThreadExecutor();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);
        final Agent agent = new Agent();
        this.pool.execute(agent);
        this.up.addActionListener(e -> agent.increase());
        this.down.addActionListener(e -> agent.decrease());
        this.stop.addActionListener(e -> {
            this.up.setEnabled(false);
            this.down.setEnabled(false);
            this.stop.setEnabled(false);
            this.pool.shutdownNow();
            agent.stopCounting();
        });
    }

    private final class Agent implements Runnable {
        private volatile boolean start;
        private volatile boolean stop;
        private volatile boolean up;
        private int counter;
        
        public Agent() {
            this.counter = 0;
            this.start = false;
            this.stop = false;
            this.up = true;
        }
        
        public void increase() {
            this.start = true;
            this.up = true;
        }
        
        public void decrease() {
            this.start = true;
            this.up = false;
        }

        public void run() {
            while (!this.stop) {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            ConcurrentGUI2.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    
                    if(this.start) {
                        if(this.up) {
                            this.counter++;
                        } else {
                            this.counter--;
                        }
                    }
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    System.out.println("interruption");
                }
            }
        }

        public void stopCounting() {
            this.stop = true;
        }
    }
}
