package it.unibo.oop.lab.reactivegui03;
import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public class AnotherConcurrentGUI extends JFrame {
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");

    public AnotherConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);
        final CountDown countDown = new CountDown();
        final Agent agent = new Agent();
        countDown.start();
        agent.start();
        this.up.addActionListener(e -> agent.increase());
        this.down.addActionListener(e -> agent.decrease());
        this.stop.addActionListener(e -> {
            this.up.setEnabled(false);
            this.down.setEnabled(false);
            this.stop.setEnabled(false);
            agent.stopCounting();
        });
        try {
            countDown.join();
            this.up.setEnabled(false);
            this.down.setEnabled(false);
            this.stop.setEnabled(false);
            agent.stopCounting();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }
    
    private final class CountDown extends Thread {
        private final int millis = 10000;
        public void run() {
            try {
                sleep(this.millis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private final class Agent extends Thread {
        private volatile boolean start;
        private volatile boolean stop;
        private volatile boolean up;
        private int counter;

        public Agent() {
            this.counter = 0;
            this.start = false;
            this.stop = false;
            this.up = true;
        }

        public void increase() {
            this.start = true;
            this.up = true;
        }

        public void decrease() {
            this.start = true;
            this.up = false;
        }

        public void run() {
            while (!this.stop) {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    if(this.start) {
                        if(this.up) {
                            this.counter++;
                        } else {
                            this.counter--;
                        }
                    }
                    sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    System.out.println("interruption");
                }
            }
        }

        public void stopCounting() {
            this.interrupt();
            this.stop = true;
        }
    }
    
}







